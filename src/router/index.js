import { createRouter, createWebHistory } from 'vue-router'


const routes = [
  {
    path: '',
    name: 'Case1',
    component: () => import("../views/Case/Case1.vue")
  },
  {
    path: '/Case1',
    name: 'Case1',
    component: () => import("../views/Case/Case1.vue")
  },
  {
    path: '/Case2',
    name: 'Case2',
    component: () => import("../views/Case/Case2.vue")
  },
  {
    path: '/Case3',
    name: 'Case3',
    component: () => import("../views/Case/Case3.vue"),
  },
  {
    path: '/Case3/ChooseFoto',
    name: 'ChooseFoto',
    component: () => import("../views/Case/ChooseFoto.vue"),
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
